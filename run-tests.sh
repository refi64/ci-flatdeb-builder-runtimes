#!/bin/bash

set -e

flatpak --user remote-add --no-gpg-verify test "$PWD/$TARGETS_REPO"
flatpak --user -y install test org.apertis.{headless,curl}.hello \
  --arch="$FLATPAK_ARCH"

ls=$(flatpak run --command=which org.apertis.headless.hello ls)
echo "##### ls: $ls"
curl=$(flatpak run --command=which org.apertis.curl.hello curl)
echo "##### curl: $curl"

